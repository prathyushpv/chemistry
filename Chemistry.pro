#-------------------------------------------------
#
# Project created by QtCreator 2014-04-16T20:02:16
#
#-------------------------------------------------

QT       += core gui

TARGET = Chemistry
TEMPLATE = app


SOURCES += main.cpp\
        chemistry.cpp

HEADERS  += chemistry.h

FORMS    += chemistry.ui
