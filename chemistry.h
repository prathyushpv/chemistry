#ifndef CHEMISTRY_H
#define CHEMISTRY_H

#include <QMainWindow>
#include <cstdlib>


namespace Ui {
    class Chemistry;
}

class Chemistry : public QMainWindow {
    Q_OBJECT
public:
    void game();
    Chemistry(QWidget *parent = 0);
    ~Chemistry();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::Chemistry *ui;

private slots:

    void on_pushButton_6_clicked();
    void on_about_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_5_clicked();
    void on_lineEdit_returnPressed();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_clicked();
    void clickan();
    void clickel();
    void clickboth();
};


#endif // CHEMISTRY_H
