#include "chemistry.h"
#include "ui_chemistry.h"
#include "QMessageBox"


int totalQuestions,questionNumber,randnum,randmode,correct,wrong;
int an[31];
QString el[31];
char* elptr;
QString z[31];
short int method=0;
int first=0;
int repet=0;
int acc;
//QMessageBox msgbox;

Chemistry::Chemistry(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Chemistry)
{
    for(int firstloop=1;firstloop<31;firstloop++)
    {
        an[firstloop]=firstloop;
    }
    correct=0;
    wrong=0;
    z[1]="1";
    z[2]="2";
    z[3]="3";
    z[4]="4";
    z[5]="5";
    z[6]="6";
    z[7]="7";
    z[8]="8";
    z[9]="9";
    z[10]="10";
    z[11]="11";
    z[12]="12";
    z[13]="13";
    z[14]="14";
    z[15]="15";
    z[16]="16";
    z[17]="17";
    z[18]="18";
    z[19]="19";
    z[20]="20";
    z[21]="21";
    z[22]="22";
    z[23]="23";
    z[24]="24";
    z[25]="25";
    z[26]="26";
    z[27]="27";
    z[28]="28";
    z[29]="29";
    z[30]="30";

    el[1]='H';
    el[2]="He";
    el[3]="Li";
    el[4]="Be";
    el[5]="B";
    el[6]="C";
    el[7]="N";
    el[8]='O';
    el[9]='F';
    el[10]="Ne";
    el[11]="Na";
    el[12]="Mg";
    el[13]="Al";
    el[14]="Si";
    el[15]="P";
    el[16]="S";
    el[17]="Cl";
    el[18]="Ar";
    el[19]="K";
    el[20]="Ca";
    el[21]="Sc";
    el[22]="Ti";
    el[23]="V";
    el[24]="Cr";
    el[25]="Mn";
    el[26]="Fe";
    el[27]="Co";
    el[28]="Ni";
    el[29]="Cu";
    el[30]="Zn";
    totalQuestions=0;

    ui->setupUi(this);
}

Chemistry::~Chemistry()
{
    delete ui;
}

void Chemistry::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void Chemistry::on_pushButton_clicked()
{

    ui->pushButton_3->setEnabled(1);
    ui->lineEdit->setEnabled(1);
    if(ui->label)
    ui->label->clear();
 ui->lineEdit->setFocus();
 ui->pushButton->setText("Next");
 game();
 if(first==0)
     first++;

 ui->totalquestions->setNum(totalQuestions);
 ui->correct->setNum(correct);
 ui->wrong->setNum(wrong);
 totalQuestions++;


}

void Chemistry::game()
{

    ui->result->clear();
    ui->result_2->clear();
    ui->lineEdit->clear();
    ui->result_2->clear();
    ui->correctans->clear();
    int prerandnum;
    setrand:
    prerandnum=rand();
    randnum=prerandnum%31;
    randmode=rand();
    randmode=randmode%2;
    if(method==1)
        randmode=0;
    else if(method==2)
        randmode=1;
    if(randnum==0||randnum==repet)
        goto setrand;
    repet=randnum;

    if(randmode==0)
    {
        ui->question1->setText("Which element has the atomic number ");
        ui->question2->setNum(randnum);

    }
    else
    {
        ui->question1->setText("What is the atomic number of ");
        ui->question2->setText(el[randnum]);

    }

}

void Chemistry::on_pushButton_3_clicked()
{   ui->pushButton_3->setDisabled(1);
    if(randmode==0)
    {
        QString ans;
        ans=ui->lineEdit->text();
        if(first==0)
            ui->result->setText("Click Start..            ");
        else
        if(ans==el[randnum])
        {
            ui->result_2->setText("Correct!");
            correct+=1;

        }
        else
        {
            ui->result->setText("Wrong!,The Correct Answer is");
            ui->correctans->setText(el[randnum]);
            wrong+=1;
        }
        ui->pushButton->setFocus();
        ui->lineEdit->clear();

    }
    else
    {
        QString answerstr;
        answerstr=ui->lineEdit->text();
        if(answerstr==z[randnum])
        {
            ui->result_2->setText("Correct!");
            correct+=1;
        }
        else
        {
            ui->result->setText("Wrong!,The Correct Answer is");
            ui->correctans->setText(z[randnum]);
            wrong+=1;
        }

    }


ui->lineEdit->setDisabled(1);


}

void Chemistry::on_pushButton_2_clicked()
{
method=1;
}

void Chemistry::on_lineEdit_returnPressed()
{
  Chemistry::on_pushButton_3_clicked();
      ui->pushButton->setFocus();

}

void Chemistry::on_pushButton_5_clicked()
{
    method=2;
}

void Chemistry::on_pushButton_4_clicked()
{
    method=0;
}

void Chemistry::clickan()
{
   method=2;
}
void Chemistry::clickel()
{
    method=1;
    ui->pushButton_2->setChecked(1);
}
void Chemistry::clickboth()
{
    method=0;
}

void Chemistry::on_about_clicked()
{
   QMessageBox msg;
   msg.setDetailedText("Prathyush PV(prathyushpv@gmail.com)");
   msg.setText("A simple quiz application");
   msg.setWindowTitle("About");
   msg.exec();
}


void Chemistry::on_pushButton_6_clicked()
{
    correct=0;
    wrong=0;
    totalQuestions=0;
    ui->totalquestions->setNum(totalQuestions);
    ui->correct->setNum(correct);
    ui->wrong->setNum(wrong);
    ui->question1->clear();
    ui->question2->clear();
    ui->pushButton_3->setDisabled(1);
    ui->pushButton->setText("Start");
    ui->label->setText("Click Start");
    ui->pushButton->setFocus();
    ui->lineEdit->setDisabled(1);
}
